import Vue from 'vue'
import Router from 'vue-router'
import Index from '@/components/Index'
import AddTeam from '@/components/AddTeam'
import EditTeam from '@/components/EditTeam'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Index',
      component: Index
    },
    {
      path: '/add-team',
      name: 'AddTeam',
      component: AddTeam
    },
    {
      path: '/edit-team/:team_slug',
      name: 'EditTeam',
      component: EditTeam
    }
  ]
})
